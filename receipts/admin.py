from django.contrib import admin
from receipts.models import ExpenseCategory
from receipts.models import Account
from receipts.models import Receipt
# Register your models here.


@admin.register(ExpenseCategory, Account, Receipt)
class ExpenseCategory(admin.ModelAdmin):
    pass


class Account(admin.ModelAdmin):
    pass


class Receipt(admin.ModelAdmin):
    pass
